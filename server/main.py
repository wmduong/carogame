#! /usr/bin/python
# File:    server/main.py

import threading
from concurrent import futures
import logging

import grpc

import caro_pb2
import caro_pb2_grpc

data = [0, 0, 0, 0, 0, 0, 0, 0, 0]
move_left = [9]

player_turn = [0]

win_lines = [
    [0, 1, 2],
    [0, 3, 6],
    [0, 4, 8],
    [1, 4, 7],
    [3, 4, 5],
    [2, 5, 8],
    [6, 7, 8]
]

lock = threading.Lock()

class Caro(caro_pb2_grpc.CaroServicer):

    def _compressed_data(self):
        s = 0
        for i, d in enumerate(data):
            s += 3 ** i * d
        return s

    def _move(self, pos, player):
        if pos < 0:
            pass
        with lock:
            if player == player_turn[0] + 1 and data[pos] == 0:
                data[pos] = player
                player_turn[0] = (player_turn[0] + 1) % 2
                move_left[0] = move_left[0] - 1

    def _check_winner(self):
        for w in win_lines:
            x = [data[i] for i in w]
            if x.count(x[0]) == len(x):
                return x[0]
        return 0 if move_left[0] > 0 else -1

    def Put(self, request, context):
        self._move(request.pos, request.player)
        return caro_pb2.BoardInfo(
            data=self._compressed_data(),
            winner=self._check_winner()
        )

    def Get(self, request, context):
        return caro_pb2.BoardInfo(
            data=self._compressed_data(),
            winner=self._check_winner()
        )

    def Delete(self, request, context):
        for i in range(9):
            data[i] = 0
        player_turn[0] = 0
        move_left[0] = 9
        return caro_pb2.BoardInfo(
            data=self._compressed_data(),
            winner=self._check_winner()
        )

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    caro_pb2_grpc.add_CaroServicer_to_server(Caro(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()

if __name__ == '__main__':
    logging.basicConfig()
    serve()
