#! /usr/bin/python
# File:    client/main.py

import grpc
import sys, pygame

import caro_pb2
import caro_pb2_grpc

pygame.init()

player = int(sys.argv[1])
print(player)

data = [0, 0, 0, 0, 0, 0, 0, 0, 0]

black = 255, 255, 255
x_img = pygame.image.load("img/x.png")
o_img = pygame.image.load("img/o.png")
b_img = pygame.image.load("img/b.png")
w_img = pygame.image.load("img/w.png")
l_img = pygame.image.load("img/l.png")
d_img = pygame.image.load("img/d.png")
o_w = 200
o_padding = 13

status = [0]

size = width, height = 601, 601
screen = pygame.display.set_mode(size)

def put(pos):
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = caro_pb2_grpc.CaroStub(channel)
        res = stub.Put(caro_pb2.Message(player=player, pos=pos))
    return res

def delete():
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = caro_pb2_grpc.CaroStub(channel)
        res = stub.Delete(caro_pb2.Void())
    return res

def get():
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = caro_pb2_grpc.CaroStub(channel)
        res = stub.Get(caro_pb2.Void())
    return res

def decompress_data(board_info):
    for i in range(9):
        data[i] = board_info % 3
        board_info = int(board_info / 3)

def handle_input():
    if status[0] == 0:
        x, y = pygame.mouse.get_pos()
        x = int(x / 200)
        y = int(y / 200)
        pos = x + y * 3
        res = put(pos)
        decompress_data(res.data)
        status[0] = res.winner
    else:
        res = delete()
        decompress_data(res.data)
        status[0] = res.winner
        print(res.data)

def update():
    res = get()
    decompress_data(res.data)
    status[0] = res.winner

def render():
    screen.fill(black)
    if status[0] == 0:
        screen.blit(b_img, (0, 0))
        for i, d in enumerate(data):
            n = i % 3
            m = int(i / 3)
            pos_n = n * o_w + o_padding
            pos_m = m * o_w + o_padding
            if d == 1: screen.blit(x_img, (pos_n, pos_m))
            if d == 2: screen.blit(o_img, (pos_n, pos_m))
    elif status[0] == player:
        screen.blit(w_img, (0, 0))
    elif status[0] == -1:
        screen.blit(d_img, (0, 0))
    else:
        screen.blit(l_img, (0, 0))
    pygame.display.flip()

        
while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            handle_input()
    update()
    render()
